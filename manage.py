from simplesunapi import create_app

app = create_app()

#gunicorn 'simplesunapi:create_app()'
#gunicorn 'myproject.app:create_app()' --workers 16
# equivalent to:
# from myproject.app import create_app
# application = create_app("production")
