Simple Sun API
--------
Get sunrise and sunset information for a given city and date.

Endpoints:
- /api/v1/city/<city_name>
- /api/v1/city/<city_name>/<date>

Example:
- /api/v1/city/Oslo
- /api/v1/city/Oslo/2015-08-31

The calculations are done by [Astral](https://astral.readthedocs.io/en/stable/index.html).

The project is an excuse to use:
- Flask 
- Pytest
- Gunicorn
- Nginx
- Docker
- Terraform

and put it all [together](https://github.com/geoHeil/pythonServing). The Flask application and tests are packaged in a Python [package](https://python-packaging.readthedocs.io/en/latest/). The tests have been [written](https://www.patricksoftwareblog.com/testing-a-flask-application-using-pytest/) using [Pytest](https://docs.pytest.org) because its simplicity and easy [integratation](https://docs.pytest.org/en/latest/goodpractices.html#tox) with "setup.py".


# Recommended books
- [Python Testing with pytest](https://pragprog.com/book/bopytest/python-testing-with-pytest)
- [The New and Improved Flask Mega-Tutorial](https://courses.miguelgrinberg.com/p/flask-mega-tutorial)


# Running the software directly

Create a virtual environment:
```sh
$ python3 -m venv venv
```

Start the virtual environment:
```sh
$ source venv/bin/activate
```

Run the tests (optional):
```sh
(venv)$ python setup.py test
```

Start the application:
```sh
(venv)$ flask run
 * Serving Flask app "simplesunapi.py"
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

## Code metrics
Generate [code metrics](https://realpython.com/python-refactoring/) report with the following command:
```sh
./generate_reports.sh

```

It will run:
- coverage
- flake8
- bandit
- wily

The result will be saved on a _reports_ folder.

## Using the API locally
You can now go to your prefered browser and type:
```
http://127.0.0.1:5000/api/v1/city/Oslo
```
Or use the command line:
```sh
curl http://127.0.0.1:5000/api/v1/city/Oslo
```
The answer will look similar to this:
```sh
{"date": "2018-10-26", "dawn": "2018-10-26 07:38:49+02:00", "dusk": "2018-10-26 18:23:08+02:00", "latitude": 59.916666666666664, "longitude": 10.75, "name": "Oslo", "noon": "2018-10-26 13:00:59+02:00", "region": "Norway", "sunrise": "2018-10-26 08:22:28+02:00", "sunset": "2018-10-26 17:39:29+02:00", "timezone": "Europe/Oslo"}
```


# Running the software using Docker
```
docker-compose build
docker-compose up -d
```

## Architecture

![General container architecture](doc/architecture.png)


## Using the API
You can now go to your prefered browser and type:
```
http://YOUR_IP_ADDRESS/api/v1/city/Oslo
```
Or use the command line:
```sh
curl http://YOUR_IP_ADDRESS/api/v1/city/Oslo
```
The answer will look similar to this:
```sh
{"date": "2018-10-26", "dawn": "2018-10-26 07:38:49+02:00", "dusk": "2018-10-26 18:23:08+02:00", "latitude": 59.916666666666664, "longitude": 10.75, "name": "Oslo", "noon": "2018-10-26 13:00:59+02:00", "region": "Norway", "sunrise": "2018-10-26 08:22:28+02:00", "sunset": "2018-10-26 17:39:29+02:00", "timezone": "Europe/Oslo"}
```


# Deployment to Digital Ocean

## Hardware provisioning
[Terraform](https://www.ottorask.com/blog/digitalocean-automation-with-terraform-and-ansible/) is responsible for the creation of the virtual machine where containers will run.

## Software provisioning
In order to automatically provision the software one should use any of these solutions:
- [Ansible](https://github.com/do-community/terraform-ansible-demo) seems nice, worth exploring in another _hackathon_ weekend.
- [Docker machine](https://www.digitalocean.com/community/tutorials/how-to-provision-and-manage-remote-docker-hosts-with-docker-machine-on-ubuntu-16-04) for _docker-compose_ setups.

Since one only has to run one simple script to provision the machine, I will let Terraform run that script.


## How to run it
Here I will describe the process I have used to make the deployment. At the end one will have a working system, but there are great improvement possibilities, like using Ansible/Docker machine as I mentioned in the previous section.

One will need a [domain pointing](https://www.digitalocean.com/docs/networking/dns/) to DigitalOcean. In this case the domain ___nordhel.com___ is used.

First of all one will need to:
- Create a DigitalOcean [access token](https://www.digitalocean.com/docs/api/create-personal-access-token/).
- Create [ssh keys](https://www.ssh.com/ssh/keygen/). I saved mine in _~/.ssh/id_rsa_digital_ocean_api_.
- [Upload](https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-account/) ssh public keys to a DigitalOcean account.

Then download and install Terraform:
```sh
wget https://releases.hashicorp.com/terraform/0.11.10/terraform_0.11.10_linux_amd64.zip
unzip terraform_0.11.10_linux_amd64.zip
sudo mv terraform /usr/local/bin/
```
Make sure you’re currently in the terraform directory:
```sh
cd deployment/terraform
```
Run Terraform:
```sh
terraform init
terraform plan
terraform apply
```
Answer _yes_ when asked. The progam will make its magic.

___NOTE___: At some point one might want to delete the droplet.
Go to the folder where the terraform files are and run these commands:
```sh
terraform init
terraform destroy
```

Once Terraform is done and the droplet ready one will see something like this:
```sh
...
digitalocean_firewall.webserver: Creation complete after 1s (ID: 5c81acbc-dbb7-4e05-bba2-5384eab6e50c)
digitalocean_domain.nordhelcom: Creation complete after 2s (ID: nordhel.com)
digitalocean_record.nordhelcom: Creating...
  domain: "" => "nordhel.com"
  fqdn:   "" => "<computed>"
  name:   "" => "nordhel.com"
  ttl:    "" => "<computed>"
  type:   "" => "A"
  value:  "" => "142.93.234.250"
```

When the script is done the containers will be running. DNS might still take a little while.
One can now use the API using a browser or the command line. For example:
```sh
curl http://nordhel.com/api/v1/city/Oslo/2018-10-25
```

Just for fun, one can log into the recently created machine with the following command:
```sh
ssh -i  ~/.ssh/id_rsa_digital_ocean_api root@142.93.234.250
```

# Final words
I think this has been a nice weekend project.
To explore:
- Try it with other cloud providers.

The weekend was productive (the weather was horrible!!) and:
- I learnt a lot. Never stop learning!
- I had a lot of fun.
- I have something to show to friends.

If you want to start playing with the possibilites DigitalOcean has to offer you can use [my referral](https://m.do.co/c/e5106eac7040) and get some dolars for free.

