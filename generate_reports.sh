#!/bin/bash

REPORTS_DIR=./reports

rm -rf $REPORTS_DIR
mkdir $REPORTS_DIR

echo "Generating reports..."

# Code test coverage
COVERAGE_DIR=$REPORTS_DIR/coverage
mkdir $COVERAGE_DIR
coverage html -d $COVERAGE_DIR

# Flake8
FLAKE8_DIR=$REPORTS_DIR/flake8
mkdir $FLAKE8_DIR
flake8 --statistics simplesunapi --benchmark --format=html --htmldir=$FLAKE8_DIR > $FLAKE8_DIR/statistics.txt

# Bandit
BANDIT_DIR=$REPORTS_DIR/bandit
mkdir $BANDIT_DIR
bandit -r ./simplesunapi/v1/ -f html -o $BANDIT_DIR/index.html

# Wily
WILY_DIR=$REPORTS_DIR/wily
mkdir $WILY_DIR
wily graph ./simplesunapi/ cyclomatic.complexity -o $WILY_DIR/cyclomatic_complexity.html
wily graph ./simplesunapi/ raw.loc -o $WILY_DIR/lines_of_code.html
wily graph ./simplesunapi/ halstead.difficulty -o $WILY_DIR/halstead_difficulty.html
wily graph ./simplesunapi/ halstead.vocabulary -o $WILY_DIR/vocabulary.html
wily graph ./simplesunapi/ maintainability.mi -o $WILY_DIR/maintainability_index.html
