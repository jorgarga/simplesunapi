import datetime
import json

def test_site_online(test_client):
    """
    Test that the site is online.
    """

    response = test_client.get('/api/v1/online')
    assert response.status_code == 200
    assert b"Online" in response.data

 
def test_oslo_today(test_client):
    """
    Test that "today" is the date when "date" is not explicitely given.
    """

    response = test_client.get('/api/v1/city/Oslo')
    data = response.data

    today = str(datetime.datetime.utcnow().date())

    assert response.status_code == 200
    assert str.encode(today) in data


def test_oslo_on_date(test_client):
    """
    Test that we get the right date when "date" is explicitely given.
    """

    response = test_client.get('/api/v1/city/Oslo/2015-08-31')
    data = response.data

    today = "2015-08-31"

    assert response.status_code == 200
    assert str.encode(today) in data


def test_oslo_complete_response(test_client):
    """
    Test the complete dictionary given as response.
    """

    response = test_client.get('/api/v1/city/Oslo/2015-08-31')
    data = json.loads(response.data)

    expected = {'name': 'Oslo',
                'region': 'Norway',
                'timezone': 'Europe/Oslo',
                'latitude': 59.916666666666664,
                'longitude': 10.75,
                'date': '2015-08-31 00:00:00',
                'dawn': '2015-08-31 05:23:02+02:00',
                'sunrise': '2015-08-31 06:08:20+02:00',
                'noon': '2015-08-31 13:17:34+02:00',
                'sunset': '2015-08-31 20:26:48+02:00',
                'dusk': '2015-08-31 21:12:06+02:00'}

    assert response.status_code == 200
    assert expected == data

