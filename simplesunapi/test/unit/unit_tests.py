"""
Unit tests for the lowest functionalities.
"""

import pytest
from simplesunapi.v1 import api as ssa

def test_verify_all_fields_exist():
    """Verify that all fields exist."""

    resp = ssa.info_for_city("Oslo")
    keys = ["name", "region", "timezone", "latitude",
            "longitude", "longitude", "longitude",
            "date", "dawn", "sunrise", "noon", "sunset", "dusk"]

    for key in keys:
        if key not in resp:
            assert False

    assert True


def test_oslo():
    """Verify that Oslo is Oslo."""

    resp = ssa.info_for_city("Oslo")
    assert resp["name"] == "Oslo"
    assert resp["region"] == "Norway"
    assert resp["timezone"] == "Europe/Oslo"
    assert resp["latitude"] == 59.916666666666664
    assert resp["longitude"] == 10.75


def test_berlin():
    """Verify that Berlin is Berlin."""

    resp = ssa.info_for_city("Berlin")
    assert resp["name"] == "Berlin"
    assert resp["region"] == "Germany"
    assert resp["timezone"] == "Europe/Berlin"
    assert resp["latitude"] == 52.5
    assert resp["longitude"] == 13.416666666666666



@pytest.mark.parametrize("correct_2015_08_31_dates",
                         [("Oslo", "2015-08-31"),
                          ("Oslo", "2015-8-31")])

def test_oslo_given_date(correct_2015_08_31_dates):
    """
    Try to get information about Oslo by using (equivalent)
    dates with different format.
    """

    resp = ssa.info_for_city(*correct_2015_08_31_dates)
    expected = {'name': 'Oslo',
                'region': 'Norway',
                'timezone': 'Europe/Oslo',
                'latitude': 59.916666666666664,
                'longitude': 10.75,
                'date': '2015-08-31 00:00:00',
                'dawn': '2015-08-31 05:23:02+02:00',
                'sunrise': '2015-08-31 06:08:20+02:00',
                'noon': '2015-08-31 13:17:34+02:00',
                'sunset': '2015-08-31 20:26:48+02:00',
                'dusk': '2015-08-31 21:12:06+02:00'}

    assert expected == resp


@pytest.mark.parametrize("incorrect_dates",
                         [("Oslo", "2015-08-32"),
                          ("Oslo", "20150831"),
                          ("Oslo", "2015 08 31"),
                          ("Oslo", "2015-0831")])


def test_oslo_given_incorrect_date(incorrect_dates):
    """Try to get information about Oslo by using incorrect dates."""

    resp = ssa.info_for_city(*incorrect_dates)
    expected = {"error": "Incorrect data format, should be YYYY-MM-DD"}
    print(resp)
    assert expected == resp


def test_non_existent_city():
    """Verify that error is returned."""

    resp = ssa.info_for_city("NonExistentCity_in_the_World")
    expected = {"error": "City not available"}

    assert expected == resp


def test_inexistent_city_valid_date():
    """Verify that error is returned."""

    resp = ssa.info_for_city("NonExistentCity_in_the_World", "2015-08-31")
    expected = {"error": "City not available"}

    assert expected == resp
