"""
Blueprint with the 1st version of the net API.
"""

from flask import current_app
from flask_jsonpify import jsonify
from simplesunapi.v1 import bp

from simplesunapi.v1 import api as ssa

@bp.route("/online")
def online():
    """
    Test route to verify that the system is online.
    """

    current_app.logger.debug("Success, we are online")
    return "Online"


@bp.route("/city/<cityname>")
def city_info(cityname):
    """
    Return sunrise/sunset information for today for the given city.
    """

    current_app.logger.debug("Get info for city: {}.".format(cityname))
    resp = ssa.info_for_city(cityname)
    return jsonify(resp)


@bp.route("/city/<cityname>/<date>")
def city_info_with_date(cityname, date):
    """
    Return sunrise/sunset information for the given city and date.
    """

    current_app.logger.debug("Get info for city: {} on {}.".format(cityname,
                                                                  date))
    resp = ssa.info_for_city(cityname, date)
    return jsonify(resp)
