from flask import Blueprint
from flask_cors import CORS

bp = Blueprint('v1', __name__)
CORS(bp)

from simplesunapi.v1 import v1
