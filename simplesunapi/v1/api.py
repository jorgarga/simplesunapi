"""
Here is the heavy lifting done and the sun information calculated.
"""

import datetime
from astral import Astral

def info_for_city(city_name:str, date_to_test:str=None) -> dict:
    """
    Returns sunrise/sunset information for a city.
    Required parameter: city name.
    Optional parameter: date. If not present today is used as date.
    Returns a dictionary with the information or with an error.

    Example:
      info_for_city("Oslo", "2018-08-31")

    """

    astral = Astral()
    astral.solar_depression = 'civil'

    response = {}

    try:
        if date_to_test is None:
            date = datetime.datetime.utcnow().date()
        else:
            date = datetime.datetime.strptime(date_to_test, '%Y-%m-%d')

        city = astral[city_name]
        sun = city.sun(date=date, local=True)

        response["name"] = city_name
        response["region"] = city.region
        response["timezone"] = city.timezone
        response["latitude"] = city.latitude
        response["longitude"] = city.longitude
        response["longitude"] = city.longitude
        response["longitude"] = city.longitude
        response["date"] = str(date)

        response["dawn"] = str(sun['dawn'])
        response["sunrise"] = str(sun['sunrise'])
        response["noon"] = str(sun['noon'])
        response["sunset"] = str(sun['sunset'])
        response["dusk"] = str(sun['dusk'])

    except KeyError:
        response["error"] = "City not available"
    except ValueError:
        response["error"] = "Incorrect data format, should be YYYY-MM-DD"

    return response
