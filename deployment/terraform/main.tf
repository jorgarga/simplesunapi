variable "digitalocean_api_token" {}
variable "ssh_fingerprint" {}

provider "digitalocean" {
token = "${var.digitalocean_api_token}"
}

resource "digitalocean_tag" "docker_experiments" {
    name = "docker_experimental"
}

resource "digitalocean_domain" "nordhelcom" {
    name = "nordhel.com"
    ip_address = "${digitalocean_droplet.docker_experiments.ipv4_address}"
}

resource "digitalocean_record" "nordhelcom" {
    name = "nordhel.com"
    type = "A"
    domain = "${digitalocean_domain.nordhelcom.name}"
    value = "${digitalocean_droplet.docker_experiments.ipv4_address}"
}

resource "digitalocean_droplet" "docker_experiments" {
    name = "docker-experiments"
    image = "docker-18-04"
    size = "s-1vcpu-1gb"
    region = "ams3"
    ipv6 = true
    private_networking = false
    tags = ["${digitalocean_tag.docker_experiments.name}"]
    ssh_keys = ["${var.ssh_fingerprint}"]

    connection {
        user = "root"
        private_key = "${file("~/.ssh/id_rsa_digital_ocean_api")}"
    }

    provisioner "file" {
        source = "${path.module}/../provision_script.sh"
        destination = "/root/provision_script.sh"
    }

    provisioner "remote-exec" {
        inline = [
          "chmod +x /root/provision_script.sh",
          "/root/provision_script.sh",
        ]
    }
} 

resource "digitalocean_firewall" "webserver" {
    name = "webserver-firewall"
    droplet_ids = ["${digitalocean_droplet.docker_experiments.id}"]

    inbound_rule = [
        {
            protocol = "tcp"
            port_range = "22"
        },
        {
            protocol = "tcp"
            port_range = "80"
        },
        {
            protocol = "tcp"
            port_range = "443"
        }
    ]

    outbound_rule = [
        {
            protocol = "tcp"
            port_range = "53"
        },
        {
            protocol = "udp"
            port_range = "53"
        }
    ]
}