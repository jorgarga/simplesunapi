#!/bin/bash
apt-get --assume-yes install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get --assume-yes install docker-ce
apt-get --assume-yes install docker-compose

useradd -ms /bin/bash noroot
sudo usermod -aG docker noroot

cd /home/noroot
sudo -u noroot git clone https://gitlab.com/jorgarga/simplesunapi.git
cd simplesunapi/
sudo -u noroot docker-compose build
sudo -u noroot docker-compose up -d
 
