FROM python:3.6

RUN useradd -ms /bin/bash gandalf
WORKDIR /home/gandalf

COPY requirements.txt requirements.txt

RUN python -m venv venv
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY simplesunapi simplesunapi
COPY manage.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP manage.py
RUN chown -R gandalf:gandalf ./

USER gandalf
EXPOSE 5000
ENTRYPOINT ["./boot.sh"]

 
