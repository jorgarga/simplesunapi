from setuptools import setup


def readme():
    with open("README.md") as f:
        return f.read()


setup(name="simplesunapi",
      version="0.1",
      description="Sunrise and sunset information",
      long_description=readme(),
      classifiers=[
          "Development Status :: 4 - Beta",
          "License :: OSI Approved :: MIT License",
          "Programming Language :: Python :: 3.6",
          "Framework :: Flask",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
          "Topic :: Scientific/Engineering :: Astronomy",
      ],
      url="http://github.com/jorgarga/simplesunapi",
      author="Jorge Garcia",
      author_email="jorgarga@gmail.com",
      license="MIT",
      packages=["simplesunapi"],
      install_requires=[
          "astral",
      ],
      setup_requires=["pytest-runner"],
      tests_require=["pytest"],
      zip_safe=False)
